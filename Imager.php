<?php
namespace vapterra\yii2vtimager;

use Exception;
use yii\base\Component;

/**
 * Created by PhpStorm.
 * Imager.php
 * @package yii2vtimager
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 07.08.15
 * @time 11:51
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
class Imager extends Component
{
    public $sizeDelimiter = 'X';
    public $paramsDelimiter = '_';
    public $resizeControllerActionName = '/site/resize/';
    public $lifetime = 0;//, ms; 0 - eternal
    public $rootPath;
    public $cachePath;
    public $storageSrc = '/upload/';
    public $emptyImagePath;
    public $defaultBgRGBAColor = [255, 255, 255, 127];
    public $defaultResizeType = Image::RESIZE_EXACTLY;
    public $resizeParameters = [];

    /*
     *
       [
            'margin_left'=>'20%',// % | px
            'margin_bottom'=>'50%',// % | px
            'limit_width'=>'50', //%
            'limit_height'=>'100',//%
            'transparency' => '50',//%
            'filters' => [],
            'type'=>'image', //image | text | both
            'file'=>'',
            'text'=>'',
            'placement'=>'tile', // emboss | tile
        ];
    */
    public $defaultWatermark = [];
    /*
     *
        [
            'top_left'=>'',
            'bottom_left'=>'',
            'top_right'=>'',
            'bottom_right'=>'',
        ];
    */
    public $defaultCorners = [];


    /*
     *
       [
            'blur' => [
                'type'=>'gaussian', // gaussian | selective
            ],
            'grayScale' => [],
            'inverse' => [],
            'colorize' => [
                'r'=>0,
                'g'=>0,
                'b'=>0,
                'a'=>0,
            ],
            'contrast' => [
                'level' => 0,
            ],
            'brightness' => [
                'level' => 0,
            ],
            'pixelate' => [
                'blockSize' => 3,
                'improved' => false,
            ],
            'rotate' => [],
       ]
     */
    public $defaultFilters = [];


    public function getResizeImage($width, $height, $path, $replaceNotFound = true, $params = [], $type = '')
    {
        $path = rawurldecode($path);
        if (!$type) {
            $type = $this->defaultResizeType;
        }

        if ($path && strpos($path, $this->storageSrc) === false) {
            $path = realpath($this->storageSrc . $path);
        }

        if ($path && strpos($path, $this->rootPath) === false) {
            $path = realpath($this->rootPath . trim( $path, DIRECTORY_SEPARATOR ) );
        }
        $src = trim( pathinfo(strtr($path, [$this->rootPath => '',$this->storageSrc=>'']), PATHINFO_DIRNAME), DIRECTORY_SEPARATOR ).DIRECTORY_SEPARATOR;
        //Имя новой картинки
        //$baseName = pathinfo($path, PATHINFO_BASENAME);
        $fileName = pathinfo($path, PATHINFO_FILENAME);
        $extension = pathinfo($path, PATHINFO_EXTENSION);
        if (empty($params)) {
            $params = [];
        }
        if (empty($params['watermark']) && !empty($this->defaultWatermark)) {
            $params['watermark'] = $this->defaultWatermark;
        }
        if (empty($params['corners']) && !empty($this->defaultCorners)) {
            $params['corners'] = $this->defaultCorners;
        }
        $names = [
            $width . $this->sizeDelimiter . $height,
            $type,
            //base64_encode(json_encode($params))
        ];
        $newFileName = $fileName . $this->paramsDelimiter . implode($this->paramsDelimiter, $names) . '.' . $extension;
        $image = null;
        try {
            $image = new Image($this->cachePath . $src . $newFileName);
            $cTime = filectime($image->getPath());
            if ($this->lifetime > 0 && $cTime < time() - $this->lifetime) {
                throw new Exception('expired cache');
            }
        } catch (Exception $e) {
            try {
                $image = new Image($path);
                $image->setArDefaultRGBABgColor($this->defaultBgRGBAColor);
                $image->resize($width, $height, $params, $type);
                if( !is_dir( $this->cachePath . $src ) ) {
                    mkdir($this->cachePath . $src,0775,true);
                }
                $image->save($this->cachePath . $src . $newFileName);
            } catch (Exception $e) {
                if (!empty($this->emptyImagePath) && $replaceNotFound && $this->emptyImagePath !== $path) {
                    return $this->getResizeImage($width, $height, $this->emptyImagePath, false, [], $type);
                }
            }
        }
        return $image;
    }

    public function getSrcResizeImage($width, $height, $path, $replaceNotFound = true, $params = [], $type = '')
    {
        $image = $this->getResizeImage($width, $height, $path, $replaceNotFound, $params, $type);
        if ($image) {
            $src = $image->getPath();
            $src = '/' . strtr($src, [$this->rootPath => '']);
            $image->__destruct();
            return $src;
        }
        return '';
    }

    public function displayResizeImage($width, $height, $path, $replaceNotFound = true, $params = [], $type = '')
    {
        $image = $this->getResizeImage($width, $height, $path, $replaceNotFound, $params, $type);
        if ($image) {
            $image->display();
            $image->__destruct();
        }
    }

    public function getPath($path)
    {
        $path = $this->emptyImagePath;
        if ($path && strpos($path, $this->storageSrc) === false) {
            $path = $this->storageSrc . $path;
        }
        if ($path && strpos($path, $this->rootPath) === false) {
            $path = realpath($this->rootPath . $path);
        }
        return $path;
    }

    public function getSrcResize($width, $height, $path, $replaceNotFound = true, $params = [], $type = '')
    {
        $path = rawurldecode($path);
        $path = trim( strtr($path, [$this->rootPath=>'',$this->storageSrc => '']), '/' );
        $path = $this->rootPath.$this->storageSrc.$path;

        if (!$type) {
            $type = $this->defaultResizeType;
        }
        $names = [
            $width . $this->sizeDelimiter . $height,
            $type,
            //base64_encode(json_encode($params))
        ];
        $paramStr = implode($this->paramsDelimiter, $names);
        $src = '';
        if (!is_file($path) && $replaceNotFound) {
            $path = $this->emptyImagePath;
        }

        if ($path && is_file($path)) {
            $src = '/' . trim( strtr($path, [$this->rootPath => '']), '/');
            $src = $this->resizeControllerActionName . $paramStr . '/' . trim( strtr($src, [$this->storageSrc => '']), '/' );
        }
        return $src;
    }
}
